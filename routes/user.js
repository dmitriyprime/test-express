var express = require('express');
var router = express.Router();

const users = require("../models/users");

// const { saveName } = require("../services/user.service");
const { isAuthorized } = require("../middlewares/auth.middleware");

router.get('/', isAuthorized, function(req, res, next) {
  res.send({
    success: "true",
    message: "users retrieved successfully",
    users: users
  });
});

router.get('/:id', isAuthorized, function(req, res) {
  const id = parseInt(req.params.id, 10);
  users.map((item) => {
    if (+item._id === id) {
      return res.status(200).send({
        success: 'true',
        message: 'user retrieved successfully',
        item,
      });
    }
  });
  return res.status(404).send({
    success: 'false',
    message: 'user does not exist',
  });

});


router.post('/', isAuthorized, function(req, res) {

  if (!req.body.name) {
    return res.status(400).send({
      success: 'false',
      message: 'name field is required'
    });
  } else if (!req.body.health) {
    return res.status(400).send({
      success: 'false',
      message: 'health field is required'
    });
  } else if (!req.body.attack) {
    return res.status(400).send({
      success: 'false',
      message: 'attack field is required'
    });
  } else if (!req.body.defense) {
    return res.status(400).send({
      success: 'false',
      message: 'defense field is required'
    });
  } else if (!req.body.source) {
    return res.status(400).send({
      success: 'false',
      message: 'source field is required'
    });
  }

  const newUser = {
    _id: users.length + 1,
    name: req.body.name,
    health: req.body.health,
    attack: req.body.attack,
    defense: req.body.defense,
    source: req.body.source
  };

  users.push(newUser);

  return res.status(201).send({
    success: 'true',
    message: 'user added successfully',
    newUser
  })

});

router.delete('/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);

  users.map((user, index) => {
    if (+user._id === id) {
      users.splice(index, 1);
      return res.status(200).send({
        success: 'true',
        message: 'user deleted successfuly',
      });
    }
  });

  return res.status(404).send({
    success: 'false',
    message: 'user not found',
  });


});

router.put('/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  let userFound;
  let itemIndex;
  users.map((user, index) => {
    if (+user._id === id) {
      userFound = user;
      itemIndex = index;
    }
  });

  if (!userFound) {
    return res.status(404).send({
      success: 'false',
      message: 'user not found',
    });
  }

  if (!req.body.name) {
    return res.status(400).send({
      success: 'false',
      message: 'name field is required'
    });
  } else if (!req.body.health) {
    return res.status(400).send({
      success: 'false',
      message: 'health field is required'
    });
  } else if (!req.body.attack) {
    return res.status(400).send({
      success: 'false',
      message: 'attack field is required'
    });
  } else if (!req.body.defense) {
    return res.status(400).send({
      success: 'false',
      message: 'defense field is required'
    });
  } else if (!req.body.source) {
    return res.status(400).send({
      success: 'false',
      message: 'source field is required'
    });
  }

  const updatedUser = {
    _id: userFound._id,
    name: req.body.name || userFound.name,
    health: req.body.health || userFound.health,
    attack: req.body.attack || userFound.attack,
    defense: req.body.defense || userFound.defense,
    source: req.body.source || userFound.source
  };

  users.splice(itemIndex, 1, updatedUser);

  return res.status(201).send({
    success: 'true',
    message: 'user updated successfully',
    updatedUser,
  });
});

module.exports = router;
