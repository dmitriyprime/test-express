var express = require('express');
var bodyParser = require('body-parser');

var indexRouter = require('./routes/index');
var userRouter = require('./routes/user');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', indexRouter);
app.use('/user', userRouter);

module.exports = app;
