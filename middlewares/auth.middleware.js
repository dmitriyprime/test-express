const isAuthorized = (req, res, next) => {
    if (
        req &&
        req.headers &&
        req.headers.authorization &&
        req.headers.authorization === 'Bearer zntuywl5m9ub52btve8xnfqypzpeyq0a'
    ) {
        next();
    } else {
        res.status(401).send({
            success: "false",
            message: "You are not allowed to get resources"
        })
    }
};

module.exports = {
    isAuthorized
}
